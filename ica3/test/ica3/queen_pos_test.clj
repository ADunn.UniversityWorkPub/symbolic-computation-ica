(ns ica3.queen-pos-test
  (:require [clojure.test :refer :all]
            [ica3.queen-pos :refer :all]))

; Static testing data
(def valid-sol [3 6 4 2 8 5 7 1])
(def line-data [1 2 3 4 5 6 7 8])

(deftest dia-points
  (testing "Using a valid n queens solution"
    (is (dia-line-points valid-sol 1 2))
    (is (dia-line-points valid-sol 3 6)))
  (testing "Using data in a line"
    (is (not (dia-line-points line-data 1 2)))
    (is (not (dia-line-points line-data 3 6)))))

(deftest dia-all
  (testing "Testing all coords for valid n queens solution"
    (is (valid-dia? valid-sol)))
  (testing "Testing all coords for data in a line"
    (is (not (valid-dia? line-data)))))

(deftest solvers
  (testing "Testing for n=8"
    (is (= 92 (count (solve-permutations 8))))
    (is (= 92 (count (solve-prog 8)))))
  (testing "Testing for n=4"
    (is (= 2 (count (solve-permutations 4))))
    (is (= 2 (count (solve-prog 4))))))
