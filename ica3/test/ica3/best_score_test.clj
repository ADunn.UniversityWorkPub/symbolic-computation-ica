(ns ica3.best-score-test
  (:require [clojure.test :refer :all]
            [ica3.best-score :refer :all]))

; Static Testing data
; each vector represents a column
(def perfect-board [
                    [1 9 17 25 33 41 49 57]
                    [2 10 18 26 34 42 50 58]
                    [3 11 19 27 35 43 51 59]
                    [4 12 20 28 36 44 52 60]
                    [5 13 21 29 37 45 53 61]
                    [6 14 22 30 38 46 54 62]
                    [7 15 23 31 39 47 55 63]
                    [8 16 24 32 40 48 56 64]])

(def ex1-board [[57 4 49 40 50 48 7 50]
                [12 3 71 23 97 84 62 40]
                [24 9 6 94 59 55 4 83]
                [41 61 58 58 22 15 32 20]
                [34 50 32 78 89 82 98 67]
                [100 71 100 83 75 63 96 2]
                [32 73 79 88 15 15 45 45]
                [46 55 29 82 8 10 97 86]])

(def sol1 [1 5 8 6 3 7 2 4])
(def sol2 [5 7 2 4 8 1 3 6])

(deftest calc-score-perfect
  (is (= 260 (calc-score sol1 perfect-board)))
  (is (= 260 (calc-score sol2 perfect-board))))

(deftest calc-score-ex1
  (is (= 535 (calc-score sol1 ex1-board)))
  (is (= 435 (calc-score sol2 ex1-board))))

