(ns ica3.queen-pos
  (:require [clojure.math.combinatorics :as combo]))

; Rows are indicated by the number columns by position in vector
; [1 2 3 4 5 6 7 8]
(defn- valid-rows? [vec-pos]
  "Tests if any 2 values in the vector are the same"
  (= (count vec-pos) (count (set vec-pos))))

(defn dia-line-points
  "Tests if 2 positions are NOT in line"
  ([vec-pos src-tst]
   (dia-line-points vec-pos (first src-tst) (first (rest src-tst))))
  ([vec-pos src tst]
   (not (=
          ; row diff
         (Math/abs (- (nth vec-pos tst) (nth vec-pos src)))
          ; col diff
         (Math/abs (- tst src))))))

(defn valid-dia?
  "Tests all positions for lines"
  ([vec-pos] (valid-dia? vec-pos
                         (filter #(not (= (first %) (first (rest %))))
                                 (combo/cartesian-product (range (count vec-pos)) (range (count vec-pos))))))
  ([vec-pos coords]
   (if (empty? coords)
     true
     (and (dia-line-points vec-pos (first coords)) (valid-dia? vec-pos (rest coords))))))

(defn- valid-state? [vec-pos]
  "Tests to make sure a vector position is valid"
  (and (valid-rows? vec-pos) (valid-dia? vec-pos)))


; Permutation solution
(defn solve-permutations [size]
  "Find all solutions to the n queens problem using permutations"
  ; Only test dia as permutations does not produce [1 1 1] vectors
  (filter ica3.queen-pos/valid-dia? (combo/permutations (map inc (range size)))))


; Progressive place solution
(defn- gen-placements
  "Returns all possible next queen positions"
  ([n state] (gen-placements n state (map inc (range n)) '()))
  ([n state remain lis-psol]
   (if (empty? remain)
     (filter valid-state? lis-psol)
     (recur n state (rest remain) (cons (conj state (first remain)) lis-psol)))))

(defn- next-states
  "Returns all possible next states from a list of all partial solutions"
  ([n lis-state] (next-states n lis-state '()))
  ([n lis-state nex]
   (if (empty? lis-state)
     nex
     (recur n (rest lis-state) (concat nex (gen-placements n (first lis-state)))))))

(defn solve-prog ([n] (solve-prog n (gen-placements n [])))
  ([n lis-states]
   (if (= (count (first lis-states)) n)
     lis-states
     (recur n (next-states n lis-states)))))