(ns ica3.best-score)

; Board scores are stored as nested vectors
; Example board defs are in best-score-test

(defn calc-score
  "Calculate the score for a specific position vector"
  ([vec-pos board] (calc-score vec-pos board 0))
  ([vec-pos board acc]
   (if (empty? vec-pos)
     acc
     (recur (rest vec-pos) (rest board) (+ acc (nth (first board) (dec (first vec-pos))))))))

(defn find-best
  "Finds the position vector which produces the greatest score"
  ([lis-vec-pos board] (find-best (rest lis-vec-pos) board (calc-score (first lis-vec-pos) board) (first lis-vec-pos)))
  ([lis-vec-pos board max-score max-vec-pos] (let [x (calc-score (first lis-vec-pos) board)]
                                               (cond
                                                 (empty? lis-vec-pos) (hash-map :vec-pos max-vec-pos :score max-score)
                                                 (> x max-score) (recur (rest lis-vec-pos) board x (first lis-vec-pos))
                                                 :else (recur (rest lis-vec-pos) board max-score max-vec-pos)))))