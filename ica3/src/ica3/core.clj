(ns ica3.core
  (:require [ica3.queen-pos :as qp]
            [ica3.best-score :as bs]))

; Boards are provided as nested vectors where each sub-vector represents a COL

(defn solve-board-perm [board]
  (bs/find-best (qp/solve-permutations (count board)) board))

(defn solve-board-prog [board]
  (bs/find-best (qp/solve-prog (count board)) board))