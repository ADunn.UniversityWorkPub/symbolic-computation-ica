(ns ica2.24abj)

; Head recursive 2.4a
(defn getSum [lis]
  (if (empty? lis)
    0
    (+ (first lis) (getSum (rest lis)))
    )
  )

(defn nested-average [lis]
  (let [lis (flatten lis)
        size (count lis)
        sum (getSum lis)]
    (if (empty? lis)
      nil
      ;return the average in integer or float
      (let [result (/ sum size)] (if (integer? result) result (float result)))
      )))

(nested-average '(10 ((30 1) 20) (8 (5 (50 7)) 9) 40))      ; 18
(nested-average '(10 ((30.2 1.34) 20.8) (8 (5 (50 7)) 9) 40)) ; 18.134
(nested-average '(1))                                       ; 1
(nested-average '(0))
(nested-average '())

; Head recursive 2.4 b

; "stats") returns the smallest, largest, count and average
;profile of numbers in a tree. Values to be returned in a map.

;Similar to 2.4, it uses flatten clojure function to solve the nested lists problem.

(defn getSum [lis]
  (if (empty? lis)
    0
    (+ (first lis) (getSum (rest lis)))
    )
  )

(defn stats [lis]
  (if (empty? lis)
    nil    ;return nil if empty
    (let [lis (flatten lis)
          smallest (apply min lis)
          largest (apply max lis)
          size (count lis)
          sum (getSum lis)
          average (/ sum size)]
      {:smallest smallest :largest largest
       :size size :sum sum :average average})
    )
  )
;tests
(stats '(10 ((30 1) 20) (8 (5 (50 7)) 9) 40))
(get (stats '(10 ((30 1) 20) (8 (5 (50 7)) 9) 40))  :average)
(stats '(1))
(stats '(0))
(get (stats '(0))  :average)
(stats '())