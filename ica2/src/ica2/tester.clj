(ns ica2.tester)
(use 'org.clojars.cognesence.matcher.core)

(defn tester [tests]
  (mfor ['(?id ?test => ?res) tests]
        (if-not (= (eval (? test)) (eval (? res)))
          (println (mout
                     '(FAILED ?id ?test => ?res)))))
  'end-of-testing)

; Tests

; Helpers
(def tree?-tests
  '(
     (1 (ica2.core/tree? 1 => false))
     (2 (ica2.core/tree? '() => false))
     (3 (ica2.core/tree? '(1) => true))
     ))

; 2.4a
(def nest-avg-flat-tests
  '(
     (1 (float (ica2.core/nested-avg-flatten ica2.data/tree-200) => (float 29539/584)))
     (2 (float (ica2.core/nested-avg-flatten ica2.data/tree-300) => (float 34600/677)))
     (3 (float (ica2.core/nested-avg-flatten '()) => 0.0))
     ))

(def nest-avg-tree-tests
  '(
     (1 (float (ica2.core/nested-avg-tree ica2.data/tree-200) => (float 29539/584)))
     (2 (float (ica2.core/nested-avg-tree ica2.data/tree-300) => (float 34600/677)))
     (3 (float (ica2.core/nested-avg-tree '()) => 0.0))
     ))

; 2.4b
(def stats-tests
  '(
     (1 (ica2.core/stats ica2.data/tree-200) => {:largest 100, :smallest 1, :count 584, :average 29539/584})
     (2 (ica2.core/stats ica2.data/tree-300) => {:largest 100, :smallest 1, :count 584, :average 29539/584})
     ))

(def stats-tests-float
  '(
     (1 (ica2.core/stats ica2.data/tree-200) => {:largest 100, :smallest 1, :count 584, :average 50.58048})
     (2 (ica2.core/stats ica2.data/tree-300) => {:largest 100, :smallest 1, :count 584, :average 50.58048})
     ))
