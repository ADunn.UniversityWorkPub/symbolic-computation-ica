(ns ica2.tail)

; ICA 2.4a ; ICA 2.4a ; ICA 2.4a ; ICA 2.4a ; ICA 2.4a
(defn nested-averageTail
  ([lis]
   (nested-averageTail (flatten lis) 0 0 0))
  ([lis sum size nums]                                      ;no 'average' argument as clojure can't divide by 0 which results in errors in first recursive calls where 'sum' and 'num' arguments are both 0 respectively
   (cond

     (and (= 0 size) (nil? (first lis))) 0                  ; if the lists are empty
     (and (empty? lis) (= 0 sum)) 0                         ; if no numbers were found in the lists
     (empty? lis) (/ sum nums)                              ;return average


     (number? (first lis))
     (recur (rest lis) (+ sum (first lis)) (+ size 1) (+ nums 1))
     :else
     (recur (rest lis) sum (+ size 1) nums)
     )))

;  ICA 2.4b ICA 2.4b ICA 2.4b ICA 2.4b
;  ICA 2.4b ICA 2.4b ICA 2.4b ICA 2.4b
(defn nested-averageTail1
  ([lis]
   (nested-averageTail1 (flatten lis) 0 0 0 Long/MAX_VALUE Long/MIN_VALUE))
  ([lis sum size nums smallest biggest]                     ;no 'average' argument as clojure can't divide by 0 which results in errors in first recursive calls where 'sum' and 'num' arguments are both 0 respectively
   (cond

     (and (= 0 size) (nil? (first lis))) 0                  ; if the lists are empty => 0
     (and (empty? lis) (= 0 sum)) nil                       ; if no numbers were found in the lists => nil
     ;   (empty? lis) (/ sum nums)                            ;return average only
     (empty? lis) {:smallestN smallest :biggestN biggest :numbers nums :size size :sum sum :average (/ sum nums)} ; return stats

     (number? (first lis))
     (nested-averageTail1 (rest lis) (+ sum (first lis)) (+ size 1) (+ nums 1)
                          (if (> smallest (first lis)) (first lis) smallest) ;getting smallest
                          (if (< biggest (first lis)) (first lis) biggest) ;getting biggest
                          )
     :else
     (nested-averageTail1 (rest lis) sum (+ size 1) nums smallest biggest)
     )))