(ns ica2.core)
(use 'criterium.core)
(use 'ica2.data)

; ############
; Some helpers
;
;
(defn tree? [lis]
  (and (seq? lis) (not (empty? lis))))

;
;#######
; 2.4a
;
(defn nested-avg-flatten
  ([nlis] (nested-avg-flatten (flatten nlis) '() 0))
  ([nlis used acc]
   (cond
     (empty? nlis) (/ acc (count used))
     :Else
     (recur (rest nlis) (cons (first nlis) used) (+ (first nlis) acc))
     )))

; Tree based head recursive solution
(defn sum-nested [nlis]
  (cond
    (empty? nlis) 0
    (tree? (first nlis)) (+ (sum-nested (first nlis)) (sum-nested (rest nlis)))
    :Else
    (+ (first nlis) (sum-nested (rest nlis)))
    )
  )

(defn count-nested [nlis]
  (cond
    (empty? nlis) 0
    (tree? (first nlis)) (+ (count-nested (first nlis)) (count-nested (rest nlis)))
    :Else
    (+ 1 (count-nested (rest nlis)))
    ))

(defn nested-avg-tree [nlis]
  (/ (sum-nested nlis) (count-nested nlis)))

; Tree based tail recursive solution
(defn sum-nested-tail
  ([nlis] (sum-nested-tail nlis 0))
  ([nlis acc] (cond
                (empty? nlis) acc
                (tree? (first nlis)) (recur (rest nlis) (+ acc (sum-nested-tail (first nlis))))
                :else
                (recur (rest nlis) (+ acc (first nlis)))
                )
    ))

(defn count-nested-tail
  ([nlis] (count-nested-tail nlis 0))
  ([nlis count] (cond
                  (empty? nlis) count
                  (tree? (first nlis)) (recur (rest nlis) (+ count (count-nested-tail (first nlis))))
                  :else
                  (recur (rest nlis) (inc count))
                  )
    ))

(defn nested-avg-tree-tail [nlis]
  (/ (sum-nested-tail nlis) (count-nested-tail nlis)))

;
; #######
; 2.4b
;

(defn get-value [nlis]
  (cond
    (empty? nlis) nil
    (number? (first nlis)) (first nlis)
    :else
    (recur (first nlis))))

(defn smallest-nested
  "Finds the smallest item in a list"
  ([lis] (smallest-nested lis (get-value lis)))
  ([lis cur]
   (cond
     (empty? lis) cur
     (tree? (first lis)) (let [smallest-tree (smallest-nested (first lis))]
                           (cond
                             (< smallest-tree cur) (recur (rest lis) smallest-tree)
                             :else
                             (recur (rest lis) cur)))
     (< (first lis) cur) (recur (rest lis) (first lis))
     :else
     (recur (rest lis) cur))
    )
  )

(defn greatest-nested
  "Finds the smallest item in a list"
  ([lis] (greatest-nested lis (first lis)))
  ([lis cur]
   (cond
     (empty? lis) cur
     (tree? (first lis)) (let [greatest-tree (greatest-nested (first lis))]
                           (cond
                             (> greatest-tree cur) (recur (rest lis) greatest-tree)
                             :else
                             (recur (rest lis) cur)))
     (> (first lis) cur) (recur (rest lis) (first lis))
     :else
     (recur (rest lis) cur))
    )
  )

(defn unified
  "Finds the value from a nested list which matches a predicate"
  ([f lis] (unified f lis (first lis)))
  ([f lis cur]
   (cond
     (empty? lis) cur
     (tree? (first lis)) (let [value (unified f (first lis))]
                           (cond
                             (f value cur) (recur f (rest lis) value)
                             :else
                             (recur f (rest lis) cur)))
     (f (first lis) cur) (recur f (rest lis) (first lis))
     :else
     (recur f (rest lis) cur))
    )
  )

(defn stats
  "Gets stats about a nested list"
  [lis] (hash-map :smallest (smallest-nested lis)
                  :largest (greatest-nested lis)
                  :count (count-nested lis)
                  :average (nested-avg-tree lis)))

; #####
; 2.4c
; #####

(defn stats-f [f lis]
  (cond
    (= f <) (smallest-nested lis)
    (= f >=) (greatest-nested lis)
    (= f =) (count-nested lis)
    (= f /) (nested-avg-tree lis)
    :else
    '(unknown symbol)))